import React, { Component } from "react";
import {BrowserRouter} from "react-router-dom";
import Header from "./Header";
import Content from "./Content";
import Footer from "./Footer";
import SideBar from "./Sidebar";
// import Tes from "./tes";

class Main extends Component {
  render() {
    return <>
    <BrowserRouter>
        {/* <Header/> */}
        <SideBar/>
        {/* <Content /> */}
        <Footer />
    </BrowserRouter>
    </>;
  }
}

export default Main;
