import React, { useContext, useState } from "react";
import { Link } from "react-router-dom";
import { UserContext } from "../context/UserContext";
import Logo from "../asset/netflix.png";


import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

const Header = () => {
  const [user, setUser] = useContext(UserContext);
  const handleLogOut = () => {
    setUser(null);
    // localStorage.removeItem("user")
    localStorage.clear()
  };

  //My Profile
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };


  const handleClose = () => {
    setAnchorEl(null);
  };

  //Movie
  const[clickedMovie, setClickedMovie] = useState(null);

  const handleNow = (e) => {
    setClickedMovie(e.currentTarget)
}

const handleTutup = () => {
    setClickedMovie(null);
};

  return (
    <header>
      
      <nav>
        <ul>
          <li>
          <MenuItem ><Link to="/">Home</Link></MenuItem>
          </li>
          {user && (
            <li>
              <MenuItem ><Link to="/game">Game</Link></MenuItem>
            </li>
          )}
          {user && (
            <li>
              <MenuItem ><Button
                aria-controls="simple-menu"
                aria-haspopup="true"
                onClick={handleNow}
              >
                Movie
              </Button></MenuItem>
              <Menu
                id="simple-menu"
                anchorEl={clickedMovie}
                keepMounted
                open={Boolean(clickedMovie)}
                onClose={handleTutup}
              >
                <MenuItem onClick={handleTutup}><Link to="/movietable">Movie Table</Link></MenuItem>
                <MenuItem onClick={handleTutup}><Link to="/movie">Movie List</Link></MenuItem> {/*bisa change pass bisa my account */}
              </Menu>
            </li>
          )}
          {user === null && (
            <li>
              <MenuItem ><Link to="/register">Sign Up</Link></MenuItem>
            </li>
          )}
          {user === null && (
            <li>
              <MenuItem ><Link to="/login">Login</Link></MenuItem>
            </li>
          )}
          {user && (
            <li>
              <MenuItem ><Button
                aria-controls="simple-menu"
                aria-haspopup="true"
                onClick={handleClick}
              >
                My Profile
              </Button></MenuItem>
              <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
              >
                <MenuItem><Link to="/profile">My Profile</Link></MenuItem>
                <MenuItem onClick={handleClose}><Link to="/changepass">Change Password</Link></MenuItem>
                {user && (<MenuItem onClick={handleLogOut}> <a style={{ cursor: "pointer" }} >
                Log Out
              </a></MenuItem>)}
              </Menu>
              
            </li>
          )}
          
        </ul>
      </nav>
    </header>
  );
};

export default Header;
