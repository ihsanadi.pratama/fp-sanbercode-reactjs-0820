import React, { useContext } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { UserContext } from "../context/UserContext";
import Home from "../component/Home";
import Game from "../component/Game";
import Movie from "../component/Movie";
import Login from "../component/Login";
import Register from "../component/Register";
import MyProfile from "../component/MyProfile";
import ChangePassword from "../component/ChangePassword";
import MovieTable from "../component/MovieTable";

const Session = () => {
  const [user] = useContext(UserContext);
  console.log((user))

  const PrivateRoute = ({ user, ...props }) => {
    if (user) {
      return <Route {...props} />;
    } else {
      return <Redirect to="/login" />;
    }
  };

  const LoginRoute = ({ user, ...props }) =>
    user ? <Redirect to="/" /> : <Route {...props} />;
  return (
    <section>
      <Switch>
        <Route component={Home} exact path="/" user={user} />
        <Route component={Game} exact path="/game" user={user} />
        <Route component={Movie} exact path="/movie" user={user} />
        <Route component={MyProfile} exact path="/profile" user={user} />
        <Route component={ChangePassword} exact path="/changepass" user={user} />
        <Route component={MovieTable}  exact path="/movietable" user={user}/>
        <Route component={Register} exact path="/register" user={user} />
        <Route component={Login} exact path="/login" user={user} />
      </Switch>
    </section>
  );
};

export default Session;
