import React, {useState, createContext} from 'react';

export const UserContext = createContext(); 

export const UserProvider = (props) => {
    const currentUser = JSON.parse(localStorage.getItem("user"));
    const initialUser = currentUser ? currentUser : null;
    const [user, setUser] = useState(initialUser);
  return (
    <UserContext.Provider value={[user, setUser]}>
      {props.children}
    </UserContext.Provider>
  );
}

