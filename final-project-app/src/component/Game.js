import React, { useState, useEffect } from "react";
import axios from "axios";

const Game = () => {
  const [game, setGame] = useState(null); 
  const [input, setInput] = useState({
    name: "",
    genre: "",
    singlePlayer: null,
    multiPlayer: null,
    platform: "",
    release: null,
    // image_url: "",
  }); 

  const [selectedId, setSelectedId] = useState(0);
  const [statusForm, setStatusForm] = useState("create"); 
  const [search, setSearch] = useState("");

  useEffect(() => {
    if (game === null) {
      axios
        .get(`https://backendexample.sanbersy.com/api/data-game`)
        .then(res => {
          setGame(
            res.data.map(el => {
              return {
                id: el.id,
                name: el.name,
                genre: el.genre,
                singlePlayer: el.singlePlayer,
                multiPlayer: el.multiPlayer,
                platform: el.platform,
                release: el.release,
                image_url: el.image_url,
              };
            })
          );
        });
    }
  }, []);
  console.log(game);

  const handleChange = (e) => {
    let typeOfInput = e.target.name;
    switch (typeOfInput) {
      case "name": {
        setInput({ ...input, name: e.target.value });
        break;
      }
      case "genre": {
        setInput({ ...input, genre: e.target.value });
        break;
      }
      case "singlePlayer": {
        setInput({ ...input, singlePlayer: e.target.value });
        break;
      }
      case "multiPlayer": {
        setInput({ ...input, multiPlayer: e.target.value });
        break;
      }
      case "platform": {
        setInput({ ...input, platform: e.target.value });
        break;
      }
      case "release": {
        setInput({ ...input, release: e.target.value });
        break;
      }
      case "image_url": {
        setInput({ ...input, image_url: e.target.value });
        break;
      }
      default: {
        break;
      }
    }
  };

  const submitForm = (e) => {

    e.preventDefault();

    let name = input.name;
    console.log(name);

    if (name.replace(/\s/g, '') !== "") {
        if(statusForm === "create"){
            axios
        .post(`https://backendexample.sanbersy.com/api/data-game`, {
          name: input.name,
          genre: input.genre,
          singlePlayer: input.singlePlayer,
          multiPlayer: input.multiPlayer,
          platform: input.platform,
          release: input.release,
        //   image_url: input.image_url
        })
        .then((res) => {
          setGame([...game, { id: res.data.id, ...input }]);
        })
    } else if (statusForm === "edit") {
      axios
        .put(`https://backendexample.sanbersy.com/api/data-game/${selectedId}`, {
          name: input.name,
          genre: input.genre,
          singlePlayer: input.singlePlayer,
          multiPlayer: input.multiPlayer,
          platform: input.platform,
          release: input.release,
        //   image_url: input.image_url,
        })
        .then(res => {
            let singleGame = game.find(el => el.id === selectedId)
            singleGame.name = input.name
            singleGame.genre = input.genre
            singleGame.singlePlayer = input.singlePlayer
            singleGame.multiPlayer = input.multiPlayer
            singleGame.platform = input.platform
            singleGame.release = input.nreleaseame
            // singleGame.image_url = input.image_url
          setGame([...game]);
        });
    }
    setStatusForm("create");
    setSelectedId(0);
    setInput({
      name: "",
      genre: "",
      singlePlayer: null,
      multiPlayer: null,
      platform: "",
      release: null,
      image_url: "",
    });
  };
}

const Action = ({ itemId }) => {
    const handleDelete = () => {
        let newGame = game.filter(el => el.id != itemId);
  
        axios
          .delete(`https://backendexample.sanbersy.com/api/data-game/${itemId}`)
          .then(res => {
            console.log(res);
          });
        setGame([...newGame]);
      };

    const handleEdit = () => {
      let singleGame = game.find(x => x.id === itemId);
      setInput({
        // id: singleGame.id,
        name: singleGame.name,
        genre: singleGame.genre,
        singlePlayer: singleGame.singlePlayer,
        multiPlayer: singleGame.multiPlayer,
        platform: singleGame.platform,
        release: singleGame.release,
        image_url: singleGame.image_url,
      });
      setSelectedId(itemId);
      setStatusForm("edit");
    };

    return (
      <>
        <td>
          <button onClick={handleEdit}>Edit</button>
          <button onClick={handleDelete}>Delete</button>
        </td>
      </>
    );
  };

  const submitSearch = (e) => {
    e.preventDefault();
    axios
      .get(`https://backendexample.sanbersy.com/api/data-game`)
      .then(res => {
        let resGame = res.data.map(el => {
          return {
            id: el.id,
            name: el.name,
            genre: el.genre,
            singlePlayer: el.singlePlayer,
            multiPlayer: el.multiPlayer,
            platform: el.platform,
            release: el.release,
            image_url: el.image_url,
          };
        });
        let filteredGame = resGame.filter(
          x => x.name.toLowerCase().indexOf(search.toLowerCase()) !== -1)
          setGame([...filteredGame]);
      });
  };

  const handleSearch = (e) => {
    setSearch(e.target.value);
  };

  return (
    <>
      <div>
        <form onSubmit={submitSearch}>
          <input type="text" value={search} onChange={handleSearch} />
          <button>Search</button>
        </form>
      </div>

      <div>
        <h1>Game List</h1>
        <table>
          <thead>
            <tr>
              <th>No</th>
              <th>Name</th>
              <th>Genre</th>
              <th>Single Player</th>
              <th>Multi Player</th>
              <th>Platform</th>
              <th>Release</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {game !== null &&
              game.map((item, index) => {
                return (
                  <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{item.name}</td>
                    <td>{item.genre}</td>
                    <td>{item.singlePlayer}</td>
                    <td>{item.multiPlayer}</td>
                    <td>{item.platform}</td>
                    <td>{item.release}</td>
                    <Action itemId={item.id} />
                  </tr>
                );
              })}
          </tbody>
        </table>
      </div>

      <div>
        <h1>Game Form</h1>
        <form onSubmit={submitForm}>
          <div>
            <label>Name:</label>
            <input
              type="text"
              name="name"
              value={input.name}
              onChange={handleChange}
            />
          </div>

          <div>
            <label>Genre:</label>
            <input
              type="text"
              name="genre"
              value={input.genre}
              onChange={handleChange}
            />
          </div>

          <div>
            <label>Single Player:</label>
            <input
              type="number"
              name="singlePlayer"
              value={input.singlePlayer}
              onChange={handleChange}
              min="1"
              max="1"
            />
          </div>

          <div>
            <label>Multi Player:</label>
            <input
              type="number"
              name="multiPlayer"
              value={input.multiPlayer}
              onChange={handleChange}
              min="0"
            />
          </div>

          <div>
            <label>Platform:</label>
            <input
              type="text"
              name="platform"
              value={input.platform}
              onChange={handleChange}
            />
          </div>

          <div>
            <label>Release:</label>
            <input
              type="number"
              name="release"
              value={input.release}
              onChange={handleChange}
              min="1958"
            />
          </div>

          <div>
            <label>Image URL:</label>
           
            <textarea cols="50" rows="3" type="text" name="image_url" value={input.image_url} onChange={handleChange}/>
          </div>

          <div>
            <button>Submit</button>
          </div>
        </form>
      </div>
    </>
  );
};

export default Game;
