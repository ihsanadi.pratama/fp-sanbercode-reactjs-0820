import React, { useState, useContext } from "react";
import { useHistory } from "react-router-dom";
import { UserContext } from "../context/UserContext";

const ChangePassword = () => {
  //    const history = useHistory();
  const [, setUser] = useContext(UserContext);
  const [input, setInput] = useState({
    oldPassword: "",
    newPassword: "",
    confirmPassword: "",
  });
//   const [isChanging, setIsChanging] = useState(false);
  const [errors, setErrors] = useState({oldPasswordErrors: "", newPasswordErrors: "", confirmPasswordErrors: ""}) //errorsnya apa nih   username: "", password: ""


//   function validateForm() {
//     //?
//     return (
//       input.oldPassword.length > 0 &&
//       input.newPassword.length > 0 &&
//       input.newPassword === input.confirmPassword
//     );
//   }

  const handleChange = (e) => {
    let value = e.target.value;
    let name = e.target.name;
    switch (name) {
      case "oldPassword": {
        setInput({ ...input, oldPassword: value });
        break;
      }
      // case "email":{
      //     setInput({ ...input, email: value})
      // }
      case "newPassword": {
        setInput({ ...input, newPassword: value });
        break;
      }
      case "confirmPassword": {
        setInput({ ...input, confirmPassword: value });
        break;
      }
      // case "confirmPassword":{ //
      //     setInput({ ...input, confirmPassword: value})
      // }
      default: {
        break;
      }
    }
  };

 

  const handleSubmit = (e) => {
    e.preventDefault();



    if (handleValidation()) {
        setUser({username: JSON.parse(localStorage.getItem("user")).username, password: input.newPassword})
        localStorage.setItem("user", JSON.stringify({username: JSON.parse(localStorage.getItem("user")).username, password: input.newPassword})
        );
        alert("mantul")
    } else {
        alert("failed bro")
    }
  };

  const handleValidation = () => {
  
    let isValid = true;


    if (!input.oldPassword) {
      isValid = false;
 
      setErrors({ ...errors, oldPasswordErrors: "Please enter your old password." });

    }

    if (input.oldPassword !== JSON.parse(localStorage.getItem("user")).password) {
        isValid = false;

        setErrors({ ...errors, oldPasswordErrors: "Old password didn't match." });

      }

    if (!input.newPassword) {
      isValid = false;
  
      setErrors({ ...errors, newPasswordErrors: "Please enter your new password." });
    }

    if (!input.confirmPassword) {
      isValid = false;
      setErrors({ ...errors, confirmPasswordErrors: "Please enter your confirm password." });
    }

    if(input.newPassword !== input.confirmPassword){
        isValid = false;
        setErrors({ ...errors, confirmPasswordErrors: "Password didn't match." });
    }

    return isValid;
  };

  return (
    <>
      <form onSubmit={handleSubmit}>
        <label>Old Password: </label>
        <input
          type="password"
          name="oldPassword"
          onChange={handleChange}
          value={input.oldPassword}
        />
        <br />
        <label>New Password: </label>
        <input
          type="password"
          name="newPassword"
          onChange={handleChange}
          value={input.newPassword}
        />
        <br />
        <label>Confirm Password: </label>
        <input
          type="password"
          name="confirmPassword"
          onChange={handleChange}
          value={input.confirmPassword}
        />
        <br />
        <button>Change Password</button>
      </form>
    </>
  );
};

export default ChangePassword;
