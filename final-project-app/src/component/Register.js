import React, { useContext, useState } from "react";
import { UserContext } from "../context/UserContext";
// import { useHistory } from "react-router-dom" //tester ae
import axios from 'axios';

const Register = () => {
    const [errors, setErrors] = useState({nameErrors: "", emailErrors: "", passwordErrors: ""}) //errorsnya apa nih   username: "", password: ""
    const[input, setInput] = useState({name: "", email: "", password: ""}) //email: "", confirmPassword: ""
    const [, setUser] = useContext(UserContext)
    

    const handleChange = (e) => {
        let value = e.target.value
        let name = e.target.name
        switch(name){
            case "name":{
                setInput({ ...input, name: value})
                break;
            }
            case "email":{
                setInput({ ...input, email: value})
                break;
            }
            // case "email":{
            //     setInput({ ...input, email: value})
            // }
            case "password":{
                setInput({ ...input, password: value})
            }
            // case "confirmPassword":{ //
            //     setInput({ ...input, confirmPassword: value})
            // }
            default:{
                break;
            }
        }
    }

    const handleValidation = (e) => {
        // let insert = input;
        let validate = {}
        let isValid = true;

        if(!input.name){ //
            isValid = false;
            // validate["username"] = "Plese enter your name." //
            setErrors({...errors, nameErrors: "Plese enter your name."})
            // console.log(errors)
            // console.log(input)
        }

        if(!input.email){ //
            isValid = false;
            // validate["username"] = "Plese enter your name." //
            setErrors({...errors, emailErrors: "Plese enter your email."})
            // console.log(errors)
            // console.log(input)
        }

        // if(!input.email){ // bisa juga input["email"]
        //     isValid = false;
        //     validate["email"] = "Plese enter your email Address" //
        // }

        // if(typeof input.email !== "undefined"){
        //     var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        //     if(!pattern.test(input.email)){ //.test
        //         isValid = false;
        //         validate["email"] = "Plese enter your email Address"
        //     }
        // }

        if(!input.password){
            isValid = false;
            // validate["password"] = "Please enter your password"
            setErrors({...errors, passwordErrors: "Please enter your password."})
            // console.log(validate["password"])
        }

        // if(!input.confirmPassword){
        //     isValid = false;
        //     validate["confirmPassword"] = "Please enter your confirm password"
        // }
        
        // if(typeof input.password !== "undefined" && typeof input.confirmPassword !== "undefined"){
        //     if(input.password != input.confirmPassword){
        //         isValid = false;
        //         validate["password"] = "Password don't match";
        //     }
        // }

        // setErrors({errors: validate}); //{}
        return isValid;
    }

    const handleSubmit = (e) => {
        // let history = useHistory();
        e.preventDefault();
        
        // if(handleValidation(e)){
            // user.push(JSON.parse(localStorage.getItem("user")));
            axios.post(`https://backendexample.sanbersy.com/api/register`, {
                // headers: {"Authorization": `Bearer ${user.token}`},
                name: input.name,
                email: input.email,
                password: input.password,
            }).then(
                (res) => {
                    console.log(res)
                    var user = res.data.user
                    var token = res.data.token
                    var currentUser = {name: user.name, email: user.email, token }
                    setUser(currentUser)
                    localStorage.setItem("user", JSON.stringify(currentUser))
                }
            ).catch((err) => {
                axios.get('/foo')
            .catch(function (error){
                if(error.response){
                    console.log(error.response.data);
                    console.log(error.response.status);
                    console.log(error.response.headers);
                }
            })
            })
            // setUser({ email: input.email, password: input.password})
            
            // localStorage.setItem("user", JSON.stringify({email: input.email, password: input.password})
            // );
            // alert("submit")
            
            
            // console.log(input)
            // console.log(JSON.parse(localStorage.getItem("user")) )
            // console.log(JSON.parse(localStorage.getItem("user")).password )
            // console.log(localStorage.getItem("user"))
            // window.location.href = "/login"
        // }else{
            // console.log(errors.username)
            // alert("failed")
        // }
    }

  return (
    <>
    <form onSubmit={handleSubmit}>
        <label>Name: </label>
        <input
          type="text"
          name="name"
          onChange={handleChange}
          value={input.name}
          placeholder="Enter name"
        />
        <p style={{color: "red"}}>{errors.nameErrors}</p>
        <br />
        <label>Email: </label>
        <input
          type="text"
          name="email"
          onChange={handleChange}
          value={input.email}
          placeholder="Enter email"
        />
        <p style={{color: "red"}}>{errors.emailErrors}</p>
        <br />
        {/* <label>Email: </label>
        <input
          type="email"
          name="email"
          onChange={handleChange}
          value={input.email}
          placeholder="Enter email"
        /> */}
        {/* <p style={{color: "red"}}>{errors.email}</p>  */}
        <br />
        <label>Password: </label>
        <input
          type="password"
          name="password"
          onChange={handleChange}
          value={input.password}
          placeholder="Enter password"
        />
        <p style={{color: "red"}}>{errors.passwordErrors}</p>
        <br />
        {/* <label>Confirm Password: </label>
        <input
          type="password"
          name="confirmPassword"
          onChange={handleChange}
          value={input.confirmPassword}
          placeholder="Enter confirm password"
        /> */}
        {/* <p style={{color: "red"}}>{errors.name}</p> */}
        <br />
        <button>Submit</button>
      </form>
    </>
  );
};

export default Register;
