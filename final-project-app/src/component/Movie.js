import React, { useState, useEffect, useContext } from "react";
import axios from "axios";
import { UserContext } from "../context/UserContext";


const Movie = () => {
  const [movie, setMovie] = useState(null);
  const [input, setInput] = useState({
    title: "",
    description: "",
    year: null,
    duration: null,
    genre: "",
    rating: null,
    review: "",
    // image_url: "",
  });

  const [selectedId, setSelectedId] = useState(0);
  const [statusForm, setStatusForm] = useState("create");
  const [search, setSearch] = useState("");
  const [user, setUser] = useContext(UserContext)

  useEffect(() => {
    if (movie === null) {
      axios
        .get(`https://backendexample.sanbersy.com/api/data-movie`)
        .then((res) => {
          setMovie(
            res.data.map(el => {
              return {
                id: el.id,
                title: el.title,
                description: el.description,
                year: el.year,
                duration: el.duration,
                genre: el.genre,
                rating: el.rating,
                review: el.review,
                image_url: el.image_url,
              };
            })
          );
        }).catch((err) => {
            alert(err)
            console.log(err)
        });
    }
  }, [movie]);
  console.log(movie);

  const handleChange = (e) => {
    let typeOfInput = e.target.name;
    switch (typeOfInput) {
      case "title": {
        setInput({ ...input, title: e.target.value });
        break;
      }
      case "description": {
        setInput({ ...input, description: e.target.value });
        break;
      }
      case "year": {
        setInput({ ...input, year: e.target.value });
        break;
      }
      case "duration": {
        setInput({ ...input, duration: e.target.value });
        break;
      }
      case "genre": {
        setInput({ ...input, genre: e.target.value });
        break;
      }
      case "rating": {
        setInput({ ...input, rating: e.target.value });
        break;
      }
      case "review": {
        setInput({ ...input, review: e.target.value });
        break;
      }
      case "image_url": {
        setInput({ ...input, image_url: e.target.value });
        break;
      }
      default: {
        break;
      }
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    let title = input.title;
    console.log(title);

    if (title.replace(/\s/g, '') !== "") {
      
      if(statusForm === "create"){
        axios
        .post(`https://backendexample.sanbersy.com/api/data-movie`, {
          title: input.title,
          description: input.description,
          year: input.year,
          duration: input.duration,
          genre: input.genre,
          rating: input.rating, 
          review: input.review,
        //   image_url: input.image_url,
        }, {headers: {Authorization: `Bearer ${user.token}`}})
        .then((res) => {
            console.log(res)
          setMovie([...movie, { id: res.data.id, ...input }]);
        }).catch(function (error) {

                if(error.response){
                    console.log(error.response.data);
                    console.log(error.response.status);
                    console.log(error.response.headers);
                }

        });
      }else if (statusForm === "edit") {
      axios
        .put(`https://backendexample.sanbersy.com/api/data-movie/${selectedId}`, {
          title: input.title,
          description: input.description,
          year: input.year,
          duration: input.duration,
          genre: input.genre,
          rating: input.rating,
          review: input.review,
          image_url: input.image_url,
        }, {headers: {Authorization: `Bearer ${user.token}`}})
        .then((res) => {
            let singleMovie = movie.find(el => el.id === selectedId)
            singleMovie.title = input.title
            singleMovie.description = input.description
            singleMovie.year = input.year
            singleMovie.duration = input.duration
            singleMovie.genre = input.genre
            singleMovie.rating = input.rating
            singleMovie.review = input.review
            // singleMovie.image_url = input.image_url
          setMovie([...movie]);
        }).catch((err) => {
            alert(err)
            console.log(err)
        })
    }

    setStatusForm("create");
    setSelectedId(0);
    setInput({
      title: "",
      description: "",
      year: null,
      duration: null,
      genre: "",
      rating: null,
      review: "",
      image_url: "",
    });
  };
}

  const Action = ({ itemId }) => {
    
    const handleDelete = () => {
      let newMovie = movie.filter((el) => el.id !== itemId); 

      axios
        .delete(`https://backendexample.sanbersy.com/api/data-game/${itemId}`,  {headers: {Authorization: `Bearer ${user.token}`}})
        .then((res) => {
          console.log(res);
        }).catch((err) => {
            alert(err)
            console.log(err)
        })
        console.log(itemId)
      setMovie([...newMovie]);
    };

    const handleEdit = () => {
      let singleMovie = movie.find((x) => x.id === itemId); 
      setInput({
        title: singleMovie.title,
        description: singleMovie.description,
        year: singleMovie.year,
        duration: singleMovie.duration,
        genre: singleMovie.genre,
        rating: singleMovie.rating,
        review: singleMovie.review,
        image_url: singleMovie.image_url,
      });
      setSelectedId(itemId);
      setStatusForm("edit");
    };

    return (
      <>
        <button onClick={handleEdit}>Edit</button>
        &nbsp;
        <button onClick={handleDelete}>Delete</button>
      </>
    );
  };

  function truncateString(str, num) {

    if (str === null) {
      return "";
    } else {
      if (str.length <= num) {
        return str;
      }
      return str.slice(0, num) + "...";
    }
  }

  const submitSearch = (e) => {
    e.preventDefault();
    axios
      .get(`https://backendexample.sanbersy.com/api/data-movie`)
      .then(res => {
        let resMovie = res.data.map(el => {
          return {
            id: el.id,
            title: el.title,
            description: el.description,
            year: el.year,
            duration: el.duration,
            genre: el.genre,
            rating: el.rating,
            review: el.review,
            image_url: el.image_url,
          };
        });
        let filteredMovie = resMovie.filter(
          (x) => x.title.toLowerCase().indexOf(search.toLowerCase()) !== -1
        ); 
        setMovie([...filteredMovie]);
      }).catch((err) => {
        alert(err)
        console.log(err)
    })
  };

  const handleChangeSearch = (e) => {
    setSearch(e.target.value);
  };

  return (
    <>
      <div>
        <form onSubmit={submitSearch}>
          <input type="text" value={search} onChange={handleChangeSearch} />
          <button>search</button>
        </form>
      </div>

      <h1>Daftar Film</h1>
      <table>
        <thead>
          <tr>
            <th>No</th>
            <th>Title</th>
            <th>Description</th>
            <th>Year</th>
            <th>Duration</th>
            <th>Genre</th>
            <th>Rating</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {movie !== null &&
            movie.map((item, index) => {
              return (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{item.title}</td>
                  <td>{item.description}</td>
                  <td>{item.year}</td>
                  <td>{item.duration}</td>
                  <td>{item.genre}</td>
                  <td>{item.rating}</td>
                  <td>
                    <Action itemId={item.id} />
                  </td>
                </tr>
              );
            })}
        </tbody>
      </table>
            

      {/* Movie Form */}
      <h1>Movie Form</h1>
      <form onSubmit={handleSubmit}>
        <div>
          <label>Title:</label>
          <input
            type="text"
            name="title"
            value={input.title}
            onChange={handleChange}
          />
          <br />
          <br />
        </div>
        <div>
          <label>Description:</label>
          <input
            type="text"
            name="description"
            value={input.description}
            onChange={handleChange}
          />
          <br />
          <br />
        </div>
        <div>
          <label>Year:</label>
          <input
            type="number"
            name="year"
            value={input.year}
            onChange={handleChange}
            min="1888"
          />
          <br />
          <br />
        </div>
        <div>
          <label>Duration:</label>
          <input
            type="number"
            name="duration"
            value={input.duration}
            onChange={handleChange}
            min="0"
          />
          <br />
          <br />
        </div>
        <div>
          <label>Genre:</label>
          <input
            type="text"
            name="genre"
            value={input.genre}
            onChange={handleChange}
          />
          <br />
          <br />
        </div>
        <div>
          <label>Rating:</label>
          <input
            type="number"
            name="rating"
            value={input.rating}
            onChange={handleChange}
            min="0"
            max="10"
            step="0.1"
          />
          <br />
          <br />
        </div>
        <div>
          <label>Review:</label>
          <input
            type="text"
            name="review"
            value={input.review}
            onChange={handleChange}
          />
          <br />
          <br />
        </div>
        <div>
          <label>Image URL: </label>
          <input
            type="text"
            name="image_url"
            value={input.image_url}
            onChange={handleChange}
          />
          <br />
          <br />
        </div>
        <button>Submit</button>
      </form>
    </>
  );
};

export default Movie;
