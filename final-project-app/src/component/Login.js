import React, { useContext, useState } from "react";
import { UserContext } from "../context/UserContext";
import axios from 'axios';

const Login = () => {
    const [errors, setErrors] = useState({emailErrors: "", passwordErrors: ""})
  const [, setUser] = useContext(UserContext);
  const [input, setInput] = useState({ email: "", password: "" });


  const handleValidation = (e) => {
    // let insert = input;
    let validate = {}
    let isValid = true;

    if(!input.email){ //
        isValid = false;
        // validate["username"] = "Plese enter your name." //
        setErrors({...errors, emailErrors: "Plese enter your email."})
        // console.log(errors)
        // console.log(input)
    }

    // if(!input.email){ // bisa juga input["email"]
    //     isValid = false;
    //     validate["email"] = "Plese enter your email Address" //
    // }

    // if(typeof input.email !== "undefined"){
    //     var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    //     if(!pattern.test(input.email)){ //.test
    //         isValid = false;
    //         validate["email"] = "Plese enter your email Address"
    //     }
    // }

    if(!input.password){
        isValid = false;
        // validate["password"] = "Please enter your password"
        setErrors({...errors, passwordErrors: "Please enter your password."})
        // console.log(validate["password"])
    }

    // if(!input.confirmPassword){
    //     isValid = false;
    //     validate["confirmPassword"] = "Please enter your confirm password"
    // }
    
    // if(typeof input.password !== "undefined" && typeof input.confirmPassword !== "undefined"){
    //     if(input.password != input.confirmPassword){
    //         isValid = false;
    //         validate["password"] = "Password don't match";
    //     }
    // }

    // setErrors({errors: validate}); //{}
    return isValid;
}

  const handleSubmit = (e) => {
    e.preventDefault();
    
    if(handleValidation(e)){
        axios.post("https://backendexample.sanbersy.com/api/user-login", {
           
            email: input.email,
            password: input.password
        }).then(
            (res) => {
                var user = res.data.user
                var token = res.data.token
                var currentUser = {name: user.name, email: user.email, token }
                    setUser(currentUser)
                    localStorage.setItem("user", JSON.stringify(currentUser))
            }
        ).catch((err) => {
            alert(err)
        })

        
    }else{
        alert("Login failed");
    }
  };

  const handleChange = (e) => {
    let value = e.target.value;
    let name = e.target.name;
    switch (name) {
      case "email": {
        setInput({ ...input, email: value });
        break;
      }
      case "password": {
        setInput({ ...input, password: value });
        break;
      }
      default: {break;}
    }
  };

  return (
    <>
   
      <form onSubmit={handleSubmit}>
        <label>Email: </label>
        <input
          type="text"
          name="email"
          onChange={handleChange}
          value={input.email}
        />
        <br />
        <label>Password: </label>
        <input
          type="password"
          name="password"
          onChange={handleChange}
          value={input.password}
        />
        <br />
        <button>Submit</button>
      </form>
    
    </>
  );
};

export default Login;
