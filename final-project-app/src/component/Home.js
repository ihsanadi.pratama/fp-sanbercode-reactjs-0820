import React, { Component } from "react";
import axios from "axios";

function minuteToHours(num) {
  var hours = num / 60;
  var rhours = Math.floor(hours);
  var minutes = (hours - rhours) * 60;
  var rminutes = Math.round(minutes);
  return (
    (rhours === 0 ? "" : rhours + " Jam") +
    (rminutes === 0 ? "" : " " + rminutes + " Menit")
  );
}

class Home extends Component {
  //mau tampilin apa ya, mungkin pilihan movie or game dibagi 2 screen [rancang ui], or tampilin gambar aja
  constructor(props) {
    super(props);
    this.state = {
      movie: [],
      game: []
    };
  }

  componentDidMount() {
      axios.get(`https://backendexample.sanbersy.com/api/data-game`)
      .then(res => {
          let game = res.data.map((el) => {
              return{
                id: el.id,
                name: el.name,
                genre: el.genre,
                singlePlayer: el.singlePlayer,
                multiPlayer: el.multiPlayer,
                platform: el.platform,
                release: el.release,
                image_url: el.image_url,
              }
          })
          this.setState({ game });
      })

    axios
      .get(`https://backendexample.sanbersy.com/api/data-movie`)
      .then((res) => {
        let movie = res.data.map((el) => {
          return {
            id: el.id,
            title: el.title,
            description: el.description,
            year: el.year,
            duration: el.duration,
            genre: el.genre,
            rating: el.rating,
            review: el.review,
            image_url: el.image_url,
          };
        });
        this.setState({ movie });
      });
  }

  render() {
    return (
      <>
        <h1>Greatest Movie List</h1>
        <div>
          {this.state.movie.map((item) => {
            return (
              <div>
                <h3>{item.title}</h3>
                <div style={{ display: "inline-block" }}>
                  <div
                    style={{
                      width: "40vw",
                      float: "left",
                      display: "inline-block",
                    }}
                  >
                    <img
                      style={{
                        width: "100%",
                        height: "300px",
                        objectFit: "cover",
                      }}
                      src={item.image_url}
                    />
                  </div>
                  <div
                    style={{
                      float: "left",
                      "font-size": "20px",
                      padding: "10px",
                      top: 0,
                      display: "inline-block",
                    }}
                  >
                    <strong>Rating: {item.rating}</strong>
                    <br />
                    <strong>Duration: {item.duration}</strong>
                    <br />
                    <strong>Genre: {item.genre}</strong>
                    <br />
                  </div>
                </div>
                <p>
                  <strong>Description</strong>:{item.description}
                  <hr />
                </p>
              </div>
            );
          })}
          <button>View All Movie</button>
        </div>

        <h1>Greatest Game List</h1>
        <div>
            { this.state.game.map((item) => {
                return(
                    <div>
                <h3>{item.name}</h3>
                <div style={{ display: "inline-block" }}>
                  <div
                    style={{
                      width: "40vw",
                      float: "left",
                      display: "inline-block",
                    }}
                  >
                    <img
                      style={{
                        width: "100%",
                        height: "300px",
                        objectFit: "cover",
                      }}
                      src={item.image_url}
                    />
                  </div>
                  <div
                    style={{
                      float: "left",
                      "font-size": "20px",
                      padding: "10px",
                      top: 0,
                      display: "inline-block",
                    }}
                  >
                    <strong>genre: {item.genre}</strong>
                    <br />
                    <strong>platform: {item.platform}</strong>
                    <br />
                    <strong>release: {item.release}</strong>
                    <br />
                  </div>
                </div>
                <p>
                  <strong>Description</strong>:{item.description}
                  <hr />
                </p>
              </div>
                )
            })}
             <button>View All Game</button>
        </div>
      </>
    );
  }
}

export default Home;
